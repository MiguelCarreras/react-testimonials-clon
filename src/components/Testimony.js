import React from 'react';
import '../css/Testimony.css';

function Testimony(props) {
    return (
      <div className='testimony-container'>
        <img 
          className='testimony-image'
          src={require(`../images/${props.image}-testimony.png`)}
          alt='Foto de Emma' />

        <div className='testimony-text-container'>
          <p className='testimony-name'>
            <strong>{props.name}</strong> en {props.country}
          </p>
          <p className='testimony-charge'>
            {props.charge} en <strong>{props.business}</strong>
            </p>
          <p className='testimony-text'>"{props.testimony}"</p>
        </div>
      </div>
    );
}

export default Testimony;